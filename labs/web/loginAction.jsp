<%-- 
    Document   : loginAction
    Created on : Aug 18, 2016, 7:30:43 PM
    Author     : Whanata
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.*"%>

<jsp:useBean id="diaryApp" class="uts.wsd.DiaryApplication" scope="application" />
<jsp:useBean id="user" class="uts.wsd.User" scope="session" />

<%
    String filePath = application.getRealPath("WEB-INF/users.xml");
    diaryApp.setFilePath(filePath);
    
    String email = request.getParameter("email");
    String password = request.getParameter("password");
    String text = "";
    
    if (diaryApp.getLogin(email, password) != null) {
        text = "Login successful. Click <a href=\"index.jsp\">here</a> to return to the main page.";
        user.setUser(diaryApp.getLogin(email, password));
    } else {
        text = "Password incorrect. Click <a href=\"login.jsp\">here</a> to try again.";
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Successful</title>
    </head>
    <body>
        <p>
            <%=text%>
        </p>
    </body>
</html>
