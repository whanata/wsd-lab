<%-- 
    Document   : edit_user
    Created on : Aug 11, 2016, 8:05:44 PM
    Author     : Whanata
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.ArrayList"%>

<jsp:useBean id="user" class="uts.wsd.User" scope="session" />

<%
    String name = request.getParameter("name");
    String email = request.getParameter("email");
    String password = request.getParameter("password");
    String gender = request.getParameter("gender");
    String favcol = request.getParameter("favcol");
    
    String[] colours = {"red", "green", "blue", "yellow", "orange", "pink"};
    ArrayList<String> colourTexts = new ArrayList<String>();
    
    String selectMale = "<input type=\"radio\" name=\"gender\" value=\"Male\">Male";
    String selectFemale = "<input type=\"radio\" name=\"gender\" value=\"Female\">Female";
    
    if (name != null && email != null && password != null && gender != null && favcol != null) {
        user.setAll(name, email, password, gender, favcol);
    } else {
        if (user.getGender().equals("Male")) {
            selectMale = "<input type=\"radio\" name=\"gender\" value=\"Male\" checked>Male";
        } else if (user.getGender().equals("Female")) {
            selectFemale = "<input type=\"radio\" name=\"gender\" value=\"Female\" checked>Female";
        }
    }
    
    for (String colour: colours) {
        String printColour = colour.substring(0, 1).toUpperCase() + colour.substring(1);
        String selected = user.getFavouriteColour().equals(colour) ? "selected " : "";
        colourTexts.add("<option value=\"" + colour + "\" " + selected + "style=\"text-transform: capitalize;\">" + printColour + "</option>");
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Account</title>
    </head>
    <body>
        <h1>My Account</h1>
        <form action="edit_user.jsp" method="post">
            <table>
                <tr>
                    <td>
                        Email
                    </td>
                    <td>
                        <input type="text" name="email" value="<%=user.getEmail()%>">
                    </td>
                </tr>
                <tr>
                    <td>
                        Full Name
                    </td>
                    <td>
                        <input type="text" name="name" value="<%=user.getName()%>">
                    </td>
                </tr>
                <tr>
                    <td>
                        Password
                    </td>
                    <td>
                        <input type="password" name="password" value="<%=user.getPassword()%>">
                    </td>
                </tr>
                <tr>
                    <td rowspan="2">
                        Gender
                    </td>
                    <td>
                        <%=selectMale%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%=selectFemale%>
                    </td>
                </tr>
                <tr>
                    <td>
                        Favourite colour
                    </td>
                    <td>
                        <select name="favcol">
                            <%
                                for (String colourText: colourTexts) {
                                    out.print(colourText);
                                }
                            %>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" value="Save">
                    </td>
                </tr>
            </table>
        </form>
        <p>
            Return to the <a href="index.jsp">main page</a>.
        </p>
    </body>
</html>
