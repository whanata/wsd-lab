<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/cd">
        <html>
            <head>
                <style>
                    table.tracklist { border: solid 1px black; border-collapse: collapse; }
                    table.tracklist td { border: solid 1px #999; }
                    .artist { font-style: italic; margin-bottom: 20px; }
                    .even { background: #fff; }
                    .odd { background: #f2f2f2; }
                </style>
            </head>
            <body>
                <xsl:apply-templates/>
                <p>Total number of tracks: <xsl:value-of select="count(tracklist/track)"/></p>
                <p>Average track rating: <xsl:value-of select="sum(tracklist/track/rating) div count(tracklist/track/rating)"/></p>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="/cd/title">
        <h1>
            <xsl:apply-templates/>
        </h1>
    </xsl:template>
    
    <xsl:template match="/cd/artist">
        <div class="artist">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="/cd/tracklist">
        <table class="tracklist">
            <thead>
                <tr>
                    <th>Track</th>
                    <th>Info</th>
                    <th>Rating</th>
                </tr>
            </thead>
            <tbody>
                <xsl:apply-templates/>
            </tbody>
        </table>
    </xsl:template>
    
    <xsl:template match="/cd/tracklist/track">
        <xsl:variable name="trackData">
            <td>#<xsl:value-of select="@id"/></td>
            <td><xsl:value-of select="time"/> - <xsl:value-of select="title"/></td>
            <td><div style="width: {rating * 20}px; height: 12px; background: blue;"></div></td>
        </xsl:variable>

        <xsl:choose>
            <xsl:when test="@id mod 2 = 1">
                <tr class="odd">
                    <xsl:copy-of select="$trackData" />
                </tr>
            </xsl:when>
            <xsl:when test="@id mod 2 = 0">
                <tr class="even">
                    <xsl:copy-of select="$trackData" />
                </tr>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>