<%-- 
    Document   : test
    Created on : Jul 30, 2016, 10:44:27 PM
    Author     : Whanata
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:useBean id="user" class="wsd.lecture2.User" scope="session" />
<jsp:setProperty name="user" property="email" value="wtjo@student.uts.edu.au" />

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <p><jsp:getProperty name="user" property="email" /></p>
        <p><%= user.getEmail() %></p>
    </body>
</html>
