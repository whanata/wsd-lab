<%-- 
    Document   : whoami
    Created on : Aug 11, 2016, 6:38:24 PM
    Author     : Whanata
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:useBean id="user" class="wsd.lecture2.User" scope="session" />

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <p>The user's email is [<%= user.getEmail() %>]</p>
    </body>
</html>
