<%-- 
    Document   : index
    Created on : Aug 11, 2016, 7:25:27 PM
    Author     : Whanata
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:useBean id="user" class="uts.wsd.User" scope="session" />

<%
    String text = "";
    String link = "";
    String accountText = "";
    
    if (user.getEmail() != null && user.getName() != null) {
        text = "You are logged in as " + user.getName() + "&lt;" + user.getEmail() + "&gt;";
        link = "<a href=\"logout.jsp\">Logout</a>";
        accountText = "<ul><li><a href=\"edit_user.jsp\">My Account</a></li></ul>";
    } else {
        text = "Your are not logged in";
        link = "<a href=\"login.jsp\">Login</a> | <a href=\"register.jsp\">Register</a>";
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Index</title>
    </head>
    <body>
        <h1>Diary Keeper</h1>
        <div style="background: #eee; border: solid 1px #333; text-align: right; width: 100%;">
            <%=text%>
        </div>
        <div style="text-align: right;">
            <%=link%>
        </div>
        <%=accountText%>
    </body>
</html>
