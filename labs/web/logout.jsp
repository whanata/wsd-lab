<%-- 
    Document   : logout
    Created on : Aug 11, 2016, 7:39:16 PM
    Author     : Whanata
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    session.invalidate();
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Logout</title>
    </head>
    <body>
        <p>
            You have been logged out. Click <a href="index.jsp">here</a> to return to the main page.
        </p>
    </body>
</html>
