<%-- 
    Document   : welcome
    Created on : Jul 30, 2016, 11:13:04 PM
    Author     : Whanata
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.*"%>

<jsp:useBean id="user" class="uts.wsd.User" scope="session" />
<jsp:useBean id="diaryApp" class="uts.wsd.DiaryApplication" scope="application" />

<%
    String filePath = application.getRealPath("WEB-INF/users.xml");
    diaryApp.setFilePath(filePath);
    
    String text = "";
    String name = request.getParameter("name");
    String email = request.getParameter("email");
    String password = request.getParameter("password");
    String gender = request.getParameter("gender");
    String favcol = request.getParameter("favcol");
    String tos = request.getParameter("tos");
    
    user.setAll(email, name, password, gender, favcol);

    diaryApp.setUserToFilepath(user);
    
    if (tos != null) {
        String nameText = "<p>Welcome, " + name + "!</p>";
        String emailText = "<p>Your email is " + email + ".</p>";
        String passwordText = "<p>Your password is " + password + ".</p>";
        String genderText = "<p>Your gender is " + gender + ".</p>";
        String favColText = "<p>Your favourite colour is " + favcol + ".</p>";
        text = nameText + emailText + passwordText + genderText + favColText;
    } else {
        String tosWarningText = "<p>Sorry, you must agree to the Terms of Service.</p>";
        String goBackText = "<p>Click <a href='/labs/register.jsp'>here</a> to go back.</p>";
        favcol = "white";
        text = tosWarningText + goBackText;
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome</title>
    </head>
    <body bgcolor="<%=favcol%>">
        <%=text%>
        <p>
            Click <a href="index.jsp">here</a> to proceed to the main page.
        </p>
    </body>
</html>
