/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.wsd.soap;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.*;
import javax.jws.*;
import javax.servlet.*;
import javax.xml.bind.*;
import javax.xml.ws.*;
import javax.xml.ws.handler.*;
import uts.wsd.*;

/**
 *
 * @author Whanata
 */
@WebService(serviceName = "diaryApp")
public class DiarySOAP {
    @Resource
    private WebServiceContext context;
    
    private DiaryApplication getDiaryApp() throws JAXBException, IOException {
        // The web server can handle requests from different clients in parallel.
        // These are called "threads".
        //
        // We do NOT want other threads to manipulate the application object at the same
        // time that we are manipulating it, otherwise bad things could happen.
        //
        // The "synchronized" keyword is used to lock the application object while
        // we're manpulating it.
        ServletContext application = (ServletContext)context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
        synchronized (application) {
            DiaryApplication diaryApp = (DiaryApplication) application.getAttribute("diaryApp");
            if (diaryApp == null) {
                diaryApp = new DiaryApplication();
                diaryApp.setFilePath(application.getRealPath("WEB-INF/users.xml"));
                application.setAttribute("diaryApp", diaryApp);
            }
            return diaryApp;
        }
    }

    @WebMethod
    public Users fetchUsers() {
        DiaryApplication diaryApp;
        try {
            diaryApp = this.getDiaryApp();
            return diaryApp.getUsers();
        } catch (JAXBException ex) {
            Logger.getLogger(DiarySOAP.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DiarySOAP.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @WebMethod
    public User fetchUser(String email) {
        DiaryApplication diaryApp;
        try {
            diaryApp = this.getDiaryApp();
            return diaryApp.getUserByEmail(email);
        } catch (JAXBException ex) {
            Logger.getLogger(DiarySOAP.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DiarySOAP.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
