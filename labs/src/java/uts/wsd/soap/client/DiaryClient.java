/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.wsd.soap.client;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Whanata
 */
public class DiaryClient {
    
    public static void main(String[] args) {
        DiaryApp locator = new DiaryApp();
        DiarySOAP diaryApp = locator.getDiarySOAPPort();
        
        while (true) {
            System.out.print("Enter email address: ");
            Scanner scanner = new Scanner(System.in);
            String email = scanner.next();
            User user = diaryApp.fetchUser(email);
            if (user == null) {
                break;
            }
            System.out.println("Found " + user.getName());
        }
        
        System.out.println("No such user found.");
    }
}
