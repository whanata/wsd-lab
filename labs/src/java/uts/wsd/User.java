/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.wsd;

/**
 *
 * @author Whanata
 */
import java.io.Serializable;
import javax.xml.bind.annotation.*;

/**
 *
 * @author Whanata
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "user")
public class User implements Serializable {
    @XmlElement(name = "email")
    private String email;
    @XmlElement(name = "name")
    private String name;
    @XmlElement(name = "password")
    private String password;
    @XmlElement(name = "gender")
    private String gender;
    @XmlElement(name = "favouriteColour")
    private String favouriteColour;
    
    public User() {}
    
    public User(String email, String name, String password, String gender, String favouriteColour) {
        this.setAll(email, name, password, gender, favouriteColour);
    }
    
    public void setAll(String email, String name, String password, String gender, String favouriteColour) {
        this.setName(name);
        this.setEmail(email);
        this.setPassword(password);
        this.setGender(gender);
        this.setFavouriteColour(favouriteColour);
    }
    
    public boolean exist() {
        if (this.getEmail() != null && this.getName() != null && this.getPassword() != null && this.getGender() != null) {
            return true;
        } else {
            return false;
        }
    }
    
    public void setUser(User user) {
        this.setAll(user.getEmail(), user.getName(), user.getPassword(), user.getGender(), user.getFavouriteColour());
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the favouriteColour
     */
    public String getFavouriteColour() {
        return favouriteColour;
    }

    /**
     * @param favouriteColour the favouriteColour to set
     */
    public void setFavouriteColour(String favouriteColour) {
        this.favouriteColour = favouriteColour;
    }
}

