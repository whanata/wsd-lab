package uts.wsd;
 
import java.util.*;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
 
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "users")
public class Users implements Serializable {
    @XmlElement(name = "user")
    private ArrayList<User> list = new ArrayList<User>();

    public ArrayList<User> getList() {
        return this.list;
    }
    public void addUser(User user) {
        this.list.add(user);
    }
    public void removeUser(User user) {
        this.list.remove(user);
    }
    public User login(String email, String password) {
        // For each user in the list...
        for (User user : this.list) {
            if (user.getEmail().equals(email) && user.getPassword().equals(password))
                return user; // Login correct. Return this user.
        }
        return null; // Login incorrect. Return null.
    }
    
    public boolean checkEmail(String email) {
        for (User user : this.list) {
            if (user.getEmail().equals(email)) {
                return true;
            }
        }
        return false;
    }
    
    public User getUserByEmail(String email) {
        for (User user : this.list) {
            if (user.getEmail().equals(email)) {
                return user;
            }
        }
        return null;
    }
}
