/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.wsd;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import javax.xml.bind.*;

/**
 *
 * @author Whanata
 */
public class DiaryApplication implements Serializable {
    private String filePath;
    private Users users;

    /**
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * @param filePath the filePath to set
     * @throws javax.xml.bind.JAXBException
     * @throws java.io.IOException
     */
    public void setFilePath(String filePath) throws JAXBException, IOException {
        this.filePath = filePath;
        this.getUsersFromFilepath();
    }
    
    public void getUsersFromFilepath() throws JAXBException, IOException {
        this.users = null;
        JAXBContext jc = JAXBContext.newInstance(Users.class);
        Unmarshaller u = jc.createUnmarshaller();
        FileInputStream fin = new FileInputStream(this.filePath);
        users = (Users)u.unmarshal(fin);
        this.setUsers(users);
    }
    
    public boolean setUserToFilepath(User user) throws JAXBException, FileNotFoundException {
        if (!this.users.checkEmail(user.getEmail())) {
            JAXBContext jc = JAXBContext.newInstance(Users.class);
            Marshaller m = jc.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);   
            this.addUser(user);
            m.marshal(this.users, new FileOutputStream(this.filePath));
            return true;
        } else {
            return false;
        }
    }
    
    public void addUser(User user) {
        this.users.addUser(user);
    }

    /**
     * @return the users
     */
    public Users getUsers() {
        return users;
    }
    
    public User getLogin(String name, String password) {  
        return this.users.login(name, password);
    }
    
    public User getUserByEmail(String email) {
        return this.users.getUserByEmail(email);
    }

    /**
     * @param users the users to set
     */
    public void setUsers(Users users) {
        this.users = users;
    }
}
