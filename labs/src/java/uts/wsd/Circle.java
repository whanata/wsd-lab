package uts.wsd;

import javax.xml.bind.annotation.*;

@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Circle {

    private double radius;

    public Circle() {
        super();
    }
    
    public Circle(double radius) {
        super();
        this.setRadius(radius);
    }

    /**
     * @return the radius
     */
    @XmlElement
    public double getRadius() {
        return radius;
    }

    /**
     * @param radius the radius to set
     */
    public void setRadius(double radius) {
        this.radius = radius;
    }

    @XmlElement
    public double getArea() {
        return Math.PI * Math.pow(this.radius, 2);
    }

    @XmlElement
    public double getCircumference() {
        return Math.PI * this.getDiameter();
    }
    
    @XmlElement
    public double getDiameter() {
        return 2 * this.radius;
    }
}
